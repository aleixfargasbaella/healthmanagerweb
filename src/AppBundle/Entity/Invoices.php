<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use ORM\OneToMany;
use ORM\JoinColumn;

/**
 * Invoices
 *
 * @ORM\Table(name="invoices")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoicesRepository")
 */
class Invoices
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=45, nullable=false, unique=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="fee_id", type="integer")
     */
    private $fee_id;

    /**
     * @var string
     *
     * @ORM\Column(name="fee_name", type="string", length=45, nullable=false)
     */
    private $fee_name;

    /**
     * @var float
     *
     * @ORM\Column(name="fee_price", type="float")
     */
    private $fee_price;

    /**
     * @var float
     *
     * @ORM\Column(name="iva", type="float")
     */
    private $fee_iva;

    /**
     * @var int
     *
     * @ORM\Column(name="patient_id", type="integer")
     */
    private $patient_id;

    /**
     * @var string
     *
     * @ORM\Column(name="patient_name", type="string", length=45)
     */
    private $patient_name;

    /**
     * @var string
     *
     * @ORM\Column(name="patient_surname", type="string", length=45, nullable=true)
     */
    private $patient_surname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="generationDate", type="datetime")
     */
    private $generationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exportedDate", type="datetime")
     */
    private $exportedDate;

    /**
     * @var int
     *
     * @ORM\Column(name="administrator", type="integer", nullable=true)
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="administrator", referencedColumnName="id")
     */
    private $administrator;

    /**
     * @var int
     *
     * @ORM\Column(name="user", type="integer")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param \string $code
     *
     * @return Invoices
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set fee_id
     *
     * @param \integer $fee_id
     *
     * @return Invoices
     */
    public function setFee_id($fee_id)
    {
        $this->fee_id = $fee_id;

        return $this;
    }

    /**
     * Get fee_id
     *
     * @return int
     */
    public function getFee_id()
    {
        return $this->code;
    }

    /**
     * Set fee_name
     *
     * @param \string $fee_name
     *
     * @return Invoices
     */
    public function setFee_name($fee_name)
    {
        $this->fee_name = $fee_name;

        return $this;
    }

    /**
     * Get fee_name
     *
     * @return int
     */
    public function getFee_name()
    {
        return $this->fee_name;
    }

    /**
     * Set fee_price
     *
     * @param \float $fee_price
     *
     * @return Invoices
     */
    public function setFee_price($fee_price)
    {
        $this->fee_price = $fee_price;

        return $this;
    }

    /**
     * Get fee_price
     *
     * @return float
     */
    public function getFee_price()
    {
        return $this->fee_price;
    }


    /**
     * Set generationDate
     *
     * @param \DateTime $generationDate
     *
     * @return Invoices
     */
    public function setGenerationDate($generationDate)
    {
        $this->generationDate = $generationDate;

        return $this;
    }

    /**
     * Get generationDate
     *
     * @return \DateTime
     */
    public function getGenerationDate()
    {
        return $this->generationDate;
    }

    /**
     * Set exportedDate
     *
     * @param \DateTime $exportedDate
     *
     * @return Invoices
     */
    public function setExportedDate($exportedDate)
    {
        $this->exportedDate = $exportedDate;

        return $this;
    }

    /**
     * Get exportedDate
     *
     * @return \DateTime
     */
    public function getExportedDate()
    {
        return $this->exportedDate;
    }

    /**
     * Set administrator
     *
     * @param integer $administrator
     *
     * @return Invoices
     */
    public function setAdministrator($administrator)
    {
        $this->administrator = $administrator;

        return $this;
    }

    /**
     * Get administrator
     *
     * @return int
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * Set user
     *
     * @return Invoices
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set fee
     *
     * @return Invoices
     */
    public function setFee($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get fee
     *
     * @return fee
     */
    public function getFee()
    {
        return $this->fee;
    }
}
