<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use ORM\OneToMany;
use ORM\JoinColumn;

/**
 * Fees
 *
 * @ORM\Table(name="fees")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FeesRepository")
 */
class Fees
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable=false)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="iva", type="float", nullable=false)
     */
    private $iva;

    /**
     * @var bool
     *
     * @ORM\Column(name="activeFee", type="boolean", options={"default":"0"}))
     */
    private $activeFee = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="defaultFee", type="boolean", options={"default":"0"}))
     */
    private $defaultFee = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registerDate", type="datetime")
     */
    private $registerDate;

    /**
     * @var int
     *
     * @ORM\Column(name="user", type="integer")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    public function __construct($user) {
        $format = 'Y-m-d H:i:s';
        $date = \DateTime::createFromFormat($format, date($format));
        $this->setRegisterDate($date);
        $this->setUser($user);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Fees
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Fees
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set iva
     *
     * @param float $iva
     *
     * @return Fees
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return float
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Set activeFee
     *
     * @param boolean $activeFee
     *
     * @return Fees
     */
    public function setActiveFee($activeFee)
    {
        $this->activeFee = $activeFee;

        return $this;
    }

    /**
     * Get activeFee
     *
     * @return bool
     */
    public function getActiveFee()
    {
        return $this->activeFee;
    }

    /**
     * Set defaultFee
     *
     * @param boolean $defaultFee
     *
     * @return Fees
     */
    public function setDefaultFee($defaultFee)
    {
        $this->defaultFee = $defaultFee;

        return $this;
    }

    /**
     * Get defaultFee
     *
     * @return bool
     */
    public function getDefaultFee()
    {
        return $this->defaultFee;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return Patients
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Get user
     *
     * @return user
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return user
     */
    public function getUser()
    {
        return $this->user;
    }
}
