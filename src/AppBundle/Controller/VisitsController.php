<?php
// src/AppBundle/Controller/CalendarController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\PatientTelephones;

use AppBundle\Form\PostType;

use AppBundle\Entity\Patients;
use AppBundle\Entity\Visits;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class VisitsController extends BaseController {
    public $section_name = 'base.global_section_visits';
    private $maxResults = 14;

    private $visit = null;

    /**
     * @Route("/visits/list/{day}", name="visits-list")
    */
    public function listVisitsAction($day = false){
        $this->init();
        if($day === false) $day = date("Y-m-d");

        $patients_names = array();
        $visits_list = $this->get_day_visits($day);

        foreach($visits_list as $visit){
            array_push($patients_names, $this->get_visit_patient_name($visit->getPatient()));
        }

        $PatientsRepository = $this->getDoctrine()->getRepository('AppBundle:Patients');
        list($lastPatientsCount, $all_patients) = $PatientsRepository->getAllPatients($this->get_logged_User_id(), 'register', null, null);

        $all_patients = $this->get_all_visits_patients();
        $all_patients_telephones = $this->get_all_visits_patients_telephones($all_patients);
        $fees = $this->get_all_visits_fees();

        $this->logger->info(var_export($visits_list, true));

        return $this->render(
            'visits/list_visits.html.twig', array(
                'error' => $this->error,
                'error_message' => $this->error_message,
                'visits_list' => $visits_list,
                'visits_patients_name' => $patients_names,
                'list_date' => $day,
                'all_patients' => $all_patients,
                'all_patients_telephones' => $all_patients_telephones,
                'all_fees' => $fees,
                'has_search' => true,
                'search_url' => $this->generateUrl('visits-search', array('fp' => 'visits-list')),
                'is_section' =>true,
                'sections' => [
                    ['url'=>$this->generateUrl('visits-list'), 'name'=>$this->getTranslatedSectionName()]
                ],
            )
        );
    }

    /**
     * @Route("/visits/week/list/{day}", name="visits-week-list")
    */
    public function listWeekVisitsAction($day = false){
        $this->init();
        $format = 'Y-m-d';
        if($day === false) $day = date($format);

        list($fromDate, $toDate) = $this->get_week_time_intervals($day);

        $patients_names = array();
        $visits_list = $this->get_week_visits($fromDate, $toDate);

        $days = array();
        for($date = clone($fromDate); $date < $toDate; $date->modify('+1 day')){
            array_push($days, $date->format($format));
        }

        foreach($visits_list as $visit){
            array_push($patients_names, $this->get_visit_patient_name($visit->getPatient()));
        }

        $all_patients = $this->get_all_visits_patients();
        $all_patients_telephones = $this->get_all_visits_patients_telephones($all_patients);
        $fees = $this->get_all_visits_fees();

        return $this->render(
            'visits/list_visits_weekly.html.twig', array(
                'error' => $this->error,
                'error_message' => $this->error_message,
                'visits_list' => $visits_list,
                'visits_patients_name' => $patients_names,
                'list_date' => $day,
                'days' => $days,
                'from_date' => $fromDate,
                'to_date' => $toDate,
                'all_patients' => $all_patients,
                'all_patients_telephones' => $all_patients_telephones,
                'all_fees' => $fees,
                'has_search' => true,
                'search_url' => $this->generateUrl('visits-search', array('fp' => 'visits-week-list')),
                'is_section' =>true,
                'sections' => [
                    ['url'=>$this->generateUrl('visits-week-list'), 'name'=>$this->getTranslatedSectionName()]
                ],
            )
        );
    }

    /**
     * @Route("/visits/search", name="visits-search")
    */
    public function searchVisitsAction(Request $request){
        $this->init();

        $result = false;
        $requested_from_page_name = $request->query->get('fp');

        $router = $this->container->get('router');
        if($router->getRouteCollection()->get($requested_from_page_name)){
            $from_page_name = $requested_from_page_name;
        } else {
            $from_page_name = 'visits-week-list';
        }

        $search = $request->query->get('search');
        if($search != null){
            $user_id = $this->get_logged_User_id();

            $VisitsRepository = $this->getDoctrine()->getRepository('AppBundle:Visits');
            $PatientsRepository = $this->getDoctrine()->getRepository('AppBundle:Patients');

            $patient_visits = array();
            $patients_list = $PatientsRepository->searchPatientsByNameOrPhone($user_id, $search);
            foreach($patients_list as $patient){
                $patient_id = $patient->getId();
                array_push($patient_visits, $VisitsRepository->getAllPatientVisits($user_id, $patient_id));
            }

            $all_patients = $this->get_all_visits_patients();
            $all_patients_telephones = $this->get_all_visits_patients_telephones($all_patients);
            $fees = $this->get_all_visits_fees();

            $result = $this->render(
                'visits/list_visits_search_results.html.twig', array(
                    'error' => $this->error,
                    'error_message' => $this->error_message,
                    'patients_list' => $patients_list,
                    'visits_list' => $patient_visits,
                    'all_patients' => $all_patients,
                    'all_patients_telephones' => $all_patients_telephones,
                    'all_fees' => $fees,
                    'has_search' => true,
                    'search_previous' => $search,
                    'search_url' => $this->generateUrl('visits-search'),
                    'is_section' =>true,
                    'sections' => [
                        ['url'=>$this->generateUrl('visits-list'), 'name'=>$this->getTranslatedSectionName()]
                    ],
                )
            );

        }
        else {
            $result = $this->redirectToRoute($from_page_name);
        }

        return $result;
    }

    /**
     * @Route("/visits/fetch/visits/", name="visits-fetch-visits")
    */
    public function fetch_VisitsAction(Request $request){
        $this->init();

        $result = 'success';
        $html = $this->translateId('visits', 'visits.error_loading_visits');

        $day = $request->request->get('new_date');
        if($day === "today") $day = date("Y-m-d");

        $patients_names = array();
        $visits_list = $this->get_day_visits($day);

        foreach($visits_list as $visit){
            array_push($patients_names, $this->get_visit_patient_name($visit->getPatient()));
            $result = ($result == 'error')? 'success':'error';
        }

        $html = $this->render(
            'visits/list_table_visits.html.twig', array(
                'visits_list' => $visits_list,
                'visits_patients_name' => $patients_names,
                'list_date' => $day,
            )
        )->getContent();

        $response = json_encode(array('status'=>$result, 'results'=>count($visits_list), 'action'=>$html));
        return new Response($response);
    }

    /**
     * @Route("/visits/fetch/week/", name="visits-fetch-week")
    */
    public function fetch_weekVisitsAction(Request $request){
        $this->init();

        $result = 'success';
        $html = $this->translateId('visits', 'visits.error_loading_visits');

        $format = 'Y-m-d';

        $day = $request->request->get('new_date');
        list($fromDate, $toDate) = $this->get_week_time_intervals($day);

        $patients_names = array();
        $visits_list = $this->get_week_visits($fromDate, $toDate);

        $days = array();
        for($date = clone($fromDate); $date < $toDate; $date->modify('+1 day')){
            array_push($days, $date->format($format));
        }

        foreach($visits_list as $visit){
            array_push($patients_names, $this->get_visit_patient_name($visit->getPatient()));
            $result = ($result == 'error')? 'success':'error';
        }

        $html = $this->render(
            'visits/list_table_visits_weekly.html.twig', array(
                'visits_list' => $visits_list,
                'visits_patients_name' => $patients_names,
                'list_date' => $day,
                'days' => $days,
                'from_date' => $fromDate,
                'to_date' => $toDate,
            )
        )->getContent();

        $response = json_encode(array('status'=>$result, 'results'=>count($visits_list), 'action'=>$html));
        return new Response($response);
    }

    /**
     * @Route("/visits/new", name="visits-new")
     */
    public function createNewVisitAction()
    {
        $this->init();
        $patients = $this->get_all_visits_patients();
        $patients_telephones = $this->get_all_visits_patients_telephones($patients);
        $fees = $this->get_all_visits_fees();

        return $this->render(
            'visits/add_visits.html.twig', array(
                'all_patients' => $patients,
                'all_patients_telephones' => $patients_telephones,
                'all_fees' => $fees,
                'error' => $this->error,
                'error_message' => $this->error_message,
                'is_section' =>true,
                'sections' => [
                    ['url'=>$this->generateUrl('visits-list'), 'name'=>$this->getTranslatedSectionName()],
                    ['url'=>'#', 'name'=>$this->translateId('visits', 'visits.section_visit_add_form')]
                ]
            )
        );
    }

    /**
     * @Route("/visits/save", name="visits-save")
     */
    public function saveNewVisitAction(Request $request){
        $this->init();

        $result = 'error';
        $action = $this->translateId('base', 'base.global_unknown_error');
        $action_listVisits = "";

        list($valid, $error_message) = $this->build_visit_entity($request);
        if($valid){
            try{
                $em = $this->getDoctrine()->getManager();
                $em->persist($this->visit);

                $em->flush();
                $result = 'success';
                $action = $this->generateUrl('visits-show', ['visit_id'=>$this->visit->getId()]);
                $action_listVisits = $this->generateUrl('visits-edit', ['visit_id'=>$this->visit->getId()]);
            } catch (UniqueConstraintViolationException $e){
                $action = $this->translateId('visits', 'visits.global_choose_another_hour');
            }
        }
        else {
            $action = $error_message;
        }
        $response = json_encode(array('status'=>$result, 'action'=>$action, 'action_listVisits'=>$action_listVisits));
        return new Response($response);
    }


    /**
     * @Route("/visits/remove", name="visits-remove")
     */
    public function removeVisitAction(Request $request){
        $this->init();

        $visits_array = $request->request->get('visits_array');
        $current_day = $request->request->get('current_day');
        $result = 'error';

        $VisitsRepository = $this->getDoctrine()->getRepository('AppBundle:Visits');
        foreach ($visits_array as $visit_id){
            try {
                $VisitsRepository->deleteVisit($this->get_logged_User_id(), $visit_id);
                $result = 'success';
                $action = $this->generateUrl('visits-list', array('day'=>$current_day));
            } catch (NotFoundHttpException $e){
                $this->logger->error($e->getMessage());
                $result = 'error';
                $action = $this->translateId('visits', 'visits.section_could_not_remove');
            }
        }

        $response = json_encode(array('status'=>$result, 'action'=>$action));
        return new Response($response);
    }

    /**
     * @Route("/visits/show/{visit_id}", name="visits-show")
    */
    public function showVisitsAction(Request $request, $visit_id){
        $this->init();

        $requested_from_page_name = $request->query->get('fp');

        $router = $this->container->get('router');
        if($router->getRouteCollection()->get($requested_from_page_name)){
            $from_page_name = $requested_from_page_name;
        } else {
            $from_page_name = 'visits-week-list';
        }

        $visit_data =  $this->get_visit($visit_id);

        if($visit_data){
            list($visit, $visit_patient_name, $visit_fee_name) = $visit_data;

            $html = $this->render(
                'visits/show_visits.html.twig', array(
                    'error' => $this->error,
                    'error_message' => $this->error_message,
                    'visit' => $visit,
                    'visit_patient_name' => $visit_patient_name,
                    'visit_fee_name' => $visit_fee_name,
                    'is_section' =>true,
                    'sections' => [
                        ['url'=>$this->generateUrl($from_page_name), 'name'=>$this->getTranslatedSectionName()],
                        ['url'=>'#', 'name'=>$visit->getVisitDate()],
                    ],
                )
            );
        } else {
            $html = $this->listVisitsAction();
        }

        return $html;
    }

    /**
     * @Route("/visits/edit/{visit_id}", name="visits-edit")
     */
    public function editVisitAction($visit_id)
    {
        $this->init();
        $visit_data =  $this->get_visit($visit_id);
        if($visit_data){
            list($visit, $visit_patient) = $visit_data;
            $patients = $this->get_all_visits_patients();
            $patients_telephones = $this->get_all_visits_patients_telephones($patients);
            $fees = $this->get_all_visits_fees();

            $result = $this->render(
                'visits/edit_visits.html.twig', array(
                    'all_patients' => $patients,
                    'all_patients_telephones' => $patients_telephones,
                    'all_fees' => $fees,
                    'visit' => $visit,
                    'visit_patient_name' => $visit_patient,
                    'error' => $this->error,
                    'error_message' => $this->error_message,
                    'is_section' =>true,
                    'sections' => [
                        ['url'=>$this->generateUrl('visits-list'), 'name'=>$this->getTranslatedSectionName()],
                        ['url'=>'#', 'name'=>$visit->getVisitDate()],
                    ],
                )
            );
        }
        else {
            $result = $this->redirectToRoute('visits-week-list');
        }

        return $result;
    }

    /**
     * @Route("/visits/save/edit", name="visits-save-edit")
     */
    public function saveEditVisitAction(Request $request){
        $this->init();
        $result = 'error';
        $action = $this->translateId('base', 'base.global_no_changes_found');
        $changes = false;

        $em = $this->getDoctrine()->getManager();

        $this->build_visit_entity($request);
        $visit_to_update = $em->getRepository('AppBundle:Visits')->find($this->visit->getId());

        $newPatient = $this->visit->getPatient();
        if($visit_to_update->getPatient() != $newPatient){
            $visit_to_update->setPatient($newPatient);
            $changes = true;
        }
        $newPhysiotherapist = $this->visit->getPhysiotherapist();
        if($visit_to_update->getPhysiotherapist() != $newPhysiotherapist){
            $visit_to_update->setPhysiotherapist($newPhysiotherapist);
            $changes = true;
        }
        $newDuration = $this->visit->getDuration();
        if($visit_to_update->getDuration() != $newDuration){
            $visit_to_update->setDuration($newDuration);
            $changes = true;
        }
        $newReason = $this->visit->getReason();
        if($visit_to_update->getReason() != $newReason){
            $visit_to_update->setReason($newReason);
            $changes = true;
        }
        $newComments = $this->visit->getComments();
        if($visit_to_update->getComments() != $newComments){
            $visit_to_update->setComments($newComments);
            $changes = true;
        }
        $newVisitDate = $this->visit->getVisitDate();
        if($visit_to_update->getVisitDate() != $newVisitDate){
            $visit_to_update->setVisitDate($newVisitDate);
            $changes = true;
        }
        $newFee = $this->visit->getFee();
        if($visit_to_update->getFee() != $newFee){
            $visit_to_update->setFee($newFee);
            $changes = true;
        }
        $newReservationDate = $this->visit->getReservationDate();
        if($visit_to_update->getReservationDate() != $newReservationDate){
            $visit_to_update->setReservationDate($newReservationDate);
            $changes = true;
        }

        if($changes){
            $em->flush();
            $result = 'success';
            $action = $this->generateUrl('visits-show', ['visit_id'=>$this->visit->getId()]);
        }

        $response = json_encode(array('status'=>$result, 'action'=>$action));
        return new Response($response);
    }

    /**
     * @Route("/visits/fetch/allVisitDates/", name="visits-fetch-allVisitDates")
     */
    public function fetch_allVisitDatesAction(Request $request){
        $this->init();
        $result = 'error';
        $action = $this->translateId('visits', 'visits.section_no_visits');

        $date = $request->request->get('dateTime');
        if($date != "" && $date !== FALSE && $date != null){
            $format = 'Y-m-d H:i:s';
            $date = \DateTime::createFromFormat($format, $date);

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                "SELECT v
                FROM AppBundle:Visits v
                WHERE v.visitDate > :start_today
                AND v.visitDate < :end_today
                AND v.user = :user_id
                ORDER BY v.visitDate ASC"
            )->setParameter('start_today', $date->format('Y-m-d 00:00:00'))
            ->setParameter('end_today', $date->format('Y-m-d 23:59:59'))
            ->setParameter('user_id', $this->get_logged_User_id());

            $visits = $query->getResult();

            if($visits){
                $result = 'success';
                $action = array();
                foreach ($visits as $visit){
                    array_push($action, $visit->getVisitDate()->format('Y-m-d H:i:s'));
                }
            }
        }

        $response = json_encode(array('status'=>$result, 'action'=>$action));
        return new Response($response);
    }

    /**
     * @Route("/visits/generate/justification/{visit_id}", name="visits-generate-justification")
     */
    public function generateVisitJustification($visit_id) {
        $this->init();
        $visit_data =  $this->get_visit($visit_id);

        if($visit_data){
            list($visit, $visit_patient) = $visit_data;

            $patients_repository = $this->getDoctrine()->getRepository('AppBundle:Patients');
            $patient_data = $patients_repository->getAllSinglePatient($this->get_logged_User_id(), $visit->getPatient());

            if($patient_data){
                $patient = $patient_data['patient'];
                $patient_addresses = $patient_data['addresses'];

                $html = $this->renderView('pdfs/visits_medical_justification_pdf.html.twig', array(
                    'visit' => $visit,
                    'patient' => $patient,
                    'patient_addresses' => $patient_addresses
                ));

                $result = new PdfResponse(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    $patient->getName() . $patient->getSurname() . '_Justification.pdf'
                );
            }
            else {
                throw new \Exception("Error Processing Request", 1);
            }
        }
        else {
            throw new \Exception("Error Processing Request", 1);
        }

        return $result;
    }

    //================ PRIVATE METHODS ==================

    private function createNewPatient($name, $surname, $phone){
        $newPatientId = FALSE;

        $patient = new Patients($this->get_logged_User_id());
        $patient->setName($name);
        $patient->setSurname($surname);
        if($phone != NULL){
            $patient->setTelephones(TRUE);
            $patientTelephones = new PatientTelephones();
            $patientTelephones->setPatient($patient->getId());
            $patientTelephones->setNumber($phone);
            $patientTelephones->setTelephoneType(1);
        } else {
            $patient->setTelephones(FALSE);
        }

        $patient->setPhoto(FALSE);
        $patient->setEmails(FALSE);
        $patient->setAddresses(FALSE);
        $patient->setOperations(FALSE);
        $patient->setAllergies(FALSE);
        $patient->setDiseases(FALSE);

        try{
            $em = $this->getDoctrine()->getManager();
            $em->persist($patient);
            $em->flush();

            $newPatientId = $patient->getId();

            if($phone != NULL){
                $patientTelephones->setPatient($newPatientId);
                $em = $this->getDoctrine()->getManager();
                $em->persist($patientTelephones);
                $em->flush();
            }
        } catch (UniqueConstraintViolationException $e){
            $this->logger->error($e->getMessage());
        }


        return $newPatientId;
    }

    private function build_visit_entity($request){
        $result = true;
        $message = "";
        $agentTimeZone = "Europe/Berlin";

        $this->visit = new Visits($this->get_logged_User_id());
        if($request->request->get('visit_id') != null){
            $this->visit->setId($request->request->get('visit_id'));
        }
        $this->visit->setPhysiotherapist(1);
        $this->visit->setDuration(60);

        if($request->request->has('save_new_patient')){
            $patient_all_new_name = trim($request->request->get('patient_all_new_name'));
            list($name, $surnames) = explode(' ', $patient_all_new_name . ' ', 2);
            $surnames = explode(' ', $surnames);

            $surname= "";
            for($i=0; $i < count($surnames); $i++){
                $surname .= ucfirst($surnames[$i]);
                if($i < count($surnames)-1){
                    $surname .= " ";
                }
            }

            $phone = $request->request->get('phone');

            $new_patient_id = $this->createNewPatient(ucfirst($name), $surname, $phone);
            if($new_patient_id){
                $this->visit->setPatient($new_patient_id);
            } else {
                $result = false;
                $message = $this->translateId('visits', 'visits.visit_patient_already_exist', array('%name%' => $name, '%surname%' => $surname));
            }
        } else {
            $this->visit->setPatient($request->request->get('patient'));
        }
        if($request->request->get('visit_date') != null){
            $date = $request->request->get('visit_date');
            $dateObject = \DateTime::createFromFormat('Y-m-d H:i:s', $date, new \DateTimeZone($agentTimeZone));
            $this->visit->setVisitDate($dateObject);
        } else {
            $result = false;
            $message = $this->translateId('visits', 'visits.error_visit_fields');
        }

        if($request->request->get('fee') != null){
            $this->visit->setFee($request->request->get('fee'));
        }

        if($request->request->get('reservation_date') != null){
            $date = $request->request->get('reservation_date');
            $dateObject = \DateTime::createFromFormat('Y-m-d H:i:s', $date, new \DateTimeZone($agentTimeZone));
            $this->visit->setReservationDate($dateObject);
        }
        $this->visit->setReason($request->request->get('reason'));
        $this->visit->setComments($request->request->get('comments'));
        $this->visit->setInvoice(0);

        return array($result, $message);
    }

    private function get_all_visits($page){
        $limit = $this->maxResults;
        $offset = $this->maxResults * ($page-1);

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT v
            FROM AppBundle:Visits v
            ORDER BY v.visitDate ASC'
        )->setFirstResult($offset)->setMaxResults($limit);

        $paginator = new Paginator($query);

        return [$paginator->count(), $paginator->getIterator()];
    }

    private function get_day_visits($day){
        $format = 'Y-m-d';
        $this->logger->info($day);
        $date = \DateTime::createFromFormat($format, $day);

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT v
            FROM AppBundle:Visits v
            WHERE v.visitDate > :from_day
            AND v.visitDate < :to_day
            AND v.user = :user_id
            ORDER BY v.visitDate ASC"
        )->setParameter('from_day', $date->format('Y-m-d 00:00:00'))
        ->setParameter('to_day', $date->format('Y-m-d 23:59:59'))
        ->setParameter('user_id', $this->get_logged_User_id());

        $visits = $query->getResult();

        return $visits;
    }

    private function get_week_visits($fromDateTime, $toDateTime){
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT v
            FROM AppBundle:Visits v
            WHERE v.visitDate > :from_day
            AND v.visitDate < :to_day
            AND v.user = :user_id
            ORDER BY v.visitDate ASC"
        )->setParameter('from_day', $fromDateTime->format('Y-m-d 00:00:00'))
        ->setParameter('to_day', $toDateTime->format('Y-m-d 23:59:59'))
        ->setParameter('user_id', $this->get_logged_User_id());

        $visits = $query->getResult();

        return $visits;
    }

    private function get_visit($visit_id){
        $result = false;
        $patient_name = false;

        $visits_repository = $this->getDoctrine()->getRepository('AppBundle:Visits');
        $visit = $visits_repository->findOneBy(
            array('id'=>$visit_id, 'user'=>$this->get_logged_User_id())
        );

        if($visit){
            $patient_name = $this->get_visit_patient_name($visit->getPatient());
            $result = [$visit, $patient_name];

            $fee = $this->FeesRepository->getFeeById($this->get_logged_User_id(), $visit->getFee());
            $fee_name = $this->translateId("visits", "visits.section_any_fee");
            if ($fee != null)
            {
                $fee_name = $fee->getName() . " - " . $fee->getPrice() . "€";
            }
            $result = [$visit, $patient_name, $fee_name];
        }

        return $result;
    }

    private function get_visit_patient_name($patient_id){
        $patient_name = '';
        $patients_repository = $this->getDoctrine()->getRepository('AppBundle:Patients');
        $patient = $patients_repository->find($patient_id);

        if (!$patient) {
            $patient_name = $this->translateId('visits', 'visits.section_unknow_patient');
        } else {
            $patient_name = $patient->getName() . " " . $patient->getSurname();
        }

        return $patient_name;
    }

    private function get_all_visits_patients(){
        $patient_name = '';
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p
            FROM AppBundle:Patients p
            WHERE p.user = :user_id
            ORDER BY p.name ASC, p.surname ASC'
        )->setParameter('user_id', $this->get_logged_User_id());

        $patients = $query->getResult();

        return $patients;
    }

    private function get_all_visits_fees(){
        $feesRepository = $this->getDoctrine()->getRepository('AppBundle:Fees');
        return $feesRepository->getAllActiveFees($this->get_logged_User_id());
    }

    private function get_all_visits_patients_telephones($all_patients){
        $patients_ids = array();
        foreach($all_patients as $patient){
            array_push($patients_ids, $patient->getId());
        }

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT pt
            FROM AppBundle:PatientTelephones pt
            WHERE pt.patient IN (:patients_ids)'
        )->setParameter('patients_ids', $patients_ids);

        $patients_telephones = $query->getResult();
        return $patients_telephones;
    }

    private function get_week_time_intervals($day){
        $format = 'Y-m-d';

        if($day === "today") $day = date($format);

        $DateTime = \DateTime::createFromFormat($format, $day);
        $fromDate = clone($DateTime);
        if($fromDate->format('D') != 'Mon'){
            $fromDate->modify('last Monday');
        }
        $toDate = $DateTime->modify('next Monday');

        return array($fromDate, $toDate);
    }
}
