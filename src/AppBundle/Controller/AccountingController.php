<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Fees;

class AccountingController extends BaseController
{
    public $section_name = 'base.global_section_accounting';


    /**
     * @Route("/accounting/invoices", name="accounting-invoices-list")
     */
    public function indexAction()
    {
        // $this->logger->info('Trace 0');
        $InvoicesRepository = $this->getDoctrine()->getRepository('Invoices');
        $allFees = $InvoicesRepository->getAllInvoices($this->get_logged_User_id());
        $allFees_count = count($allFees);

        // $this->logger->info(var_export($allFees, true));
        return $this->render(
            'accounting/list_invoices', array(
                'accounting_list' =>$allFees,
                'is_section' =>true,
                'sections' => [
                    ['url'=>$this->generateUrl('#'), 'name'=>$this->getTranslatedSectionName()]
                ]
            )
        );
    }
}
