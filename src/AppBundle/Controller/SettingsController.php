<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SettingsController extends Controller
{
    private $logger;
    private $section_name = 'base.global_section_settings';

    private function getTranslatedSectionName(){
        return $this->get('translator')->trans($this->section_name, array(), 'base');
    }

    private function get_logged_User_id(){
        $user = $this->getUser();
        return $user->getId();
    }

    /**
     * @Route("/settings", name="settings-list")
     */
    public function indexAction(Request $request)
    {
        return $this->render(
            'settings/list_settings.html.twig', array(

                'is_section' =>true,
                'sections' => [
                    ['url'=>"#", 'name'=>$this->getTranslatedSectionName()]
                ]
            )
        );
    }
}
