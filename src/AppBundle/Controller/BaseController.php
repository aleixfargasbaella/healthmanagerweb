<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\AddressTypes;
use AppBundle\Entity\Allergies;
use AppBundle\Entity\Diseases;
use AppBundle\Entity\EmailTypes;
use AppBundle\Entity\Fees;
use AppBundle\Entity\Invoices;
use AppBundle\Entity\Operations;
use AppBundle\Entity\PatientAddress;
use AppBundle\Entity\PatientAllergies;
use AppBundle\Entity\PatientDiseases;
use AppBundle\Entity\PatientEmails;
use AppBundle\Entity\PatientOperations;
use AppBundle\Entity\Patients;
use AppBundle\Entity\PatientTelephones;
use AppBundle\Entity\TelephoneTypes;
use AppBundle\Entity\Visits;
use AppBundle\Entity\WaitingList;

class BaseController extends Controller
{
    /* Utils */
    /**
     * @var object|\Symfony\Bridge\Monolog\Logger
     */
    public $logger;

    /**
     * @var object|\Symfony\Component\Translation\DataCollectorTranslator|\Symfony\Component\Translation\IdentityTranslator
     */
    public $translator;
    public $section_name = ''; //'base.global_section_sectionName';
    public $currentUserId;

    /* Repository objects */
    public $AddressTypesRepository;
    public $AllergiesRepository;
    public $DiseasesRepository;
    public $EmailTypesRepository;
    public $FeesRepository;
    public $InvoicesRepository;
    public $OperationsRepository;
    public $PatientAddressRepository;
    public $PatientAllergiesRepository;
    public $PatientDiseasesRepository;
    public $PatientEmailsRepository;
    public $PatientOperationsRepository;
    public $PatientsRepository;
    public $PatientTelephonesRepository;
    public $TelephoneTypesRepository;
    public $VisitsRepository;
    public $WaitingListRepository;

    /* Entity objects */
    public $AddressTypes;
    public $Allergies;
    public $Diseases;
    public $EmailTypes;
    public $Fees;
    public $Invoices;
    public $Operations;
    public $PatientAddress;
    public $PatientAllergies;
    public $PatientDiseases;
    public $PatientEmails;
    public $PatientOperations;
    public $Patients;
    public $PatientTelephones;
    public $TelephoneTypes;
    public $Visits;
    public $WaitingList;
    
    public $error = false;
    public $error_message = '';

    public function init(){
        // Utils
        $this->logger       = $this->get('logger');
        $this->translator   = $this->get('translator');
        $this->currentUserId = $this->get_logged_User_id();

        // Repositories
        $this->AddressTypesRepository = $this->getDoctrine()->getRepository('AppBundle:AddressTypes');
        $this->AllergiesRepository = $this->getDoctrine()->getRepository('AppBundle:Allergies');
        $this->DiseasesRepository = $this->getDoctrine()->getRepository('AppBundle:Diseases');
        $this->EmailTypesRepository = $this->getDoctrine()->getRepository('AppBundle:EmailTypes');
        $this->FeesRepository = $this->getDoctrine()->getRepository('AppBundle:Fees');
        $this->InvoicesRepository = $this->getDoctrine()->getRepository('AppBundle:Invoices');
        $this->OperationsRepository = $this->getDoctrine()->getRepository('AppBundle:Operations');
        $this->PatientAddressRepository = $this->getDoctrine()->getRepository('AppBundle:PatientAddress');
        $this->PatientAllergiesRepository = $this->getDoctrine()->getRepository('AppBundle:PatientAllergies');
        $this->PatientDiseasesRepository = $this->getDoctrine()->getRepository('AppBundle:PatientDiseases');
        $this->PatientEmailsRepository = $this->getDoctrine()->getRepository('AppBundle:PatientEmails');
        $this->PatientOperationsRepository = $this->getDoctrine()->getRepository('AppBundle:PatientOperations');
        $this->PatientsRepository = $this->getDoctrine()->getRepository('AppBundle:Patients');
        $this->PatientTelephonesRepository = $this->getDoctrine()->getRepository('AppBundle:PatientTelephones');
        $this->TelephoneTypesRepository = $this->getDoctrine()->getRepository('AppBundle:TelephoneTypes');
        $this->VisitsRepository = $this->getDoctrine()->getRepository('AppBundle:Visits');
        $this->WaitingListRepository = $this->getDoctrine()->getRepository('AppBundle:WaitingList');
        
        // Entities
        $this->AddressTypes = new AddressTypes($this->currentUserId);
        $this->Allergies = new Allergies($this->currentUserId);
        $this->Diseases = new Diseases($this->currentUserId);
        $this->EmailTypes = new EmailTypes($this->currentUserId);
        $this->Fees = new Fees($this->currentUserId);
        $this->Invoices = new Invoices($this->currentUserId);
        $this->Operations = new Operations($this->currentUserId);
        $this->PatientAddress = new PatientAddress($this->currentUserId);
        $this->PatientAllergies = new PatientAllergies($this->currentUserId);
        $this->PatientDiseases = new PatientDiseases($this->currentUserId);
        $this->PatientEmails = new PatientEmails($this->currentUserId);
        $this->PatientOperations = new PatientOperations($this->currentUserId);
        $this->Patients = new Patients($this->currentUserId);
        $this->PatientTelephones = new PatientTelephones($this->currentUserId);
        $this->TelephoneTypes = new TelephoneTypes($this->currentUserId);
        $this->Visits = new Visits($this->currentUserId);
        $this->WaitingList = new WaitingList($this->currentUserId);
    }

    public function getTranslatedSectionName(){
        return $this->translator->trans($this->section_name, array(), 'base');
    }

    public function translateId($domain, $id){
        return $this->get('translator')->trans($id, array(), $domain);
    }

    public function get_logged_User_id(){
        $user = $this->getUser();
        return $user->getId();
    }

}
