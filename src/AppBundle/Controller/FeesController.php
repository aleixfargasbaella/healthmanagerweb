<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Fees;

class FeesController extends BaseController
{
    public $section_name = 'base.global_section_fees';


    /**
     * @Route("/settings/fees/list", name="settings-fees-list")
     */
    public function indexAction()
    {
        $this->init();

        $allFees = $this->FeesRepository->getAllFees($this->get_logged_User_id());
        $allFees_count = count($allFees);

        // $this->logger->info(var_export($allFees, true));
        return $this->render(
            'fees/list_fees.html.twig', array(
                'fees_list' =>$allFees,
                'total_fees_number' =>$allFees_count,
                'is_section' =>true,
                'sections' => [
                    ['url'=>$this->generateUrl('settings-list'), 'name'=>$this->get('translator')->trans('base.global_section_settings', array(), 'base')],
                    ['url'=>$this->generateUrl('homepage'), 'name'=>$this->getTranslatedSectionName()]
                ]
            )
        );
    }

    /**
     * @Route("/settings/fees/show/{fee_id}", name="settings-fees-show")
     */
    public function showFeeAction($fee_id)
    {
        $this->init();
        list($fee, $patients_list) = $this->FeesRepository->getFeeDetails($this->get_logged_User_id(), $fee_id);

        if($fee) {
            $result = $this->render(
                'fees/show_fees.html.twig', array(
                    'fee'=>$fee,
                    'patients_list'=>$patients_list,
                    'error' => $this->error,
                    'error_message' => $this->error_message,
                    'is_section' =>true,
                    'sections' => [
                        ['url'=>$this->generateUrl('settings-list'), 'name'=>$this->get('translator')->trans('base.global_section_settings', array(), 'base')],
                        ['url'=>$this->generateUrl('settings-fees-list'), 'name'=>$this->getTranslatedSectionName()],
                        ['url'=>'#','name'=>$fee['name']]
                    ]
                )
            );
        }
        else {
            $result = $this->redirectToRoute('settings-fees-list');
        }

        return $result;
    }

    /**
     * @Route("/settings/fees/edit", name="settings-fees-edit")
     */
    public function editFeeAction(Request $request)
    {
        $this->init();
        // TODO: Poder editar dades tarifa i pacients que apliquen
    }

    /**
     * @Route("/settings/fees/save", name="settings-fees-save")
     */
    public function saveFeeAction(Request $request)
    {
        $this->init();

        $success = false;
        $message = "Unknown error";

        $hasName = false;
        $hasPrice = false;
        $hasIva = false;

        $this->logger->info('Trace 0');

        if($request->query->get('fee_name') != null){
            $this->Fees->setName($request->query->get('fee_name'));
            $hasName = true;
        }
        if($request->query->get('fee_price') != null){
            $this->Fees->setPrice(intval($request->query->get('fee_price')));
            $hasPrice = true;
        }
        if($request->query->get('fee_iva') != null){
            $this->Fees->setIva(intval($request->query->get('fee_iva')));
            $hasIva = true;
        }
        if($request->query->get('fee_active') != null){
            if($request->query->get('fee_active') == 'on') {
                $this->Fees->setActiveFee(true);
            }
        }
        if($request->query->get('fee_default') != null){
            if($request->query->get('fee_default') == 'on') {
                $this->Fees->setDefaultFee(true);
            }
        }

        if($hasName && $hasPrice && $hasIva) {
            $fee_id = $this->FeesRepository->createNewFee($this->Fees);
            if($fee_id){
                $success = true;
                $message = $fee_id;
            }
        } else {
            $success = false;
            $message = "Not all required fields are written";
        }

        $this->logger->info('success = ' . $success);
        $this->logger->info('message = ' . $message);

        return $this->redirectToRoute('settings-fees-list');
    }

    /**
     * @Route("/settings/fees/remove", name="settings-fees-remove")
     */
    public function deleteFeeAction(Request $request)
    {
        $this->init();
        $result = 'error';
        $action = $this->translateId('fees', 'fees.section_multiple_assigns_error');

        $fees_array = $request->request->get('fees_array');
        $this->logger->info("trace 0");
        $this->logger->info("trace 1: ".$this->currentUserId);
        try {
            if ($this->FeesRepository->deleteFees($this->currentUserId, $fees_array)) {
                $result = 'success';
                $action = $this->generateUrl('settings-fees-list');
            } else {
                $action = $this->translateId('fees', 'fees.section_could_not_remove_error');
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            $action = $this->translateId('fees', $e->getMessage());
        }

        $response = json_encode(array('status'=>$result, 'action'=>$action));
        return new Response($response);
    }
}
