HealthManagerWeb
================

A Symfony project.

* commit styling: https://github.com/slashsBin/styleguide-git-commit-message

### System Requirements and configurations ###

* apache2
* php7.0
* php7.0-mysql
Add at following lines at the end of the file `/etc/mysql/my.cnf`:
```
[mysqld]
sql-mode="STRICT_ALL_TABLES"
```
* php7.0-mbstring
* php7.0-intl (reboot required)
* wkhtmltopdf (to be able to export to pdf)
    In ubuntu (https://gist.github.com/brunogaspar/bd89079245923c04be6b0f92af431c10):
    ```
    sudo apt-get update
    sudo apt-get install xvfb libfontconfig wkhtmltopdf
    ```

    In Mac: `brew install wkhtmltopdf`
* Modify php.ini to set timezone `date.timezone = "Europe/Berlin"`
* run `$ composer install`

### Run ###
```
php bin/console --force server:start 0.0.0.0:8000
```

### Manage Database ###
- Gestor per a generar una nova entitat:
```
php bin/console doctrine:generate:entity
//php bin/console doctrine:generate:entities Tutorial //Investigar
```

- Creació de la db:
```
php bin/console doctrine:database:create
```

- Esborrar l'estructuració de la db:
```
php bin/console doctrine:database:drop --force
```

- Actualitzar la db en funció de les entitats:
```
php bin/console doctrine:schema:update --force
```

### Debugar internacionalització ###
http://stackoverflow.com/questions/27910549/translations-not-working-in-symfony2

- Netejar cache:
```
php bin/console cache:clear
```

- Debbug translator - change lang for the lang you want to debbug, like 'ca', 'es', 'en'
	
```
php bin/console debug:translation lang
```