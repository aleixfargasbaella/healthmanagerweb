var reader = false;
var file = false;

$(document).ready(function () {
    form_visits_datepicker();
    upload_picture();
});

function form_visits_datepicker(){
    $('#datetimepicker1').datetimepicker({
        locale: moment.locale(get_locale()),
//        pickTime: false,
        viewMode: 'years',
//        debug:true,
        ignoreReadonly: true,
        format: 'YYYY-MM-DD'
    });
}
function imageOrientation(src) {
    var orientation,
    img = new Image();
    img.src = src;

    if (img.naturalWidth > img.naturalHeight) {
        orientation = 'landscape';
    } else if (img.naturalWidth < img.naturalHeight) {
        orientation = 'portrait';
    } else {
        orientation = 'portrait';
    }

    return orientation;
}

function readURL(input) {
    if (input.files && input.files[0]) {
        file = input.files[0];
        if($('#profile-picture').hasClass('landscape')){
            $('#profile-picture').removeClass('landscape');
        }
        if($('#profile-picture').hasClass('portrait')){
            $('#profile-picture').removeClass('portrait');
        }

        reader = new FileReader();
        reader.onload = function(e) {
            var orientation = imageOrientation(e.target.result)
            $('#profile-picture').addClass(orientation);
            $('#profile-picture').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function upload_picture(){
    $("#photo").change(function() {
        readURL(this);
    });
}
