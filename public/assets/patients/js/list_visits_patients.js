var patients_selected = [];

$(document).ready(function () {
    add_show_visit_listener();
});

//=================== LIST FUNCTIONS ===================
function add_show_visit_listener(){
    $('.patient-visit').click(function(){
        var url = $(this).attr('url');
        window.location.href = url;
    });
}
//=================== END LIST FUNCTIONS ===================

//=================== REMOVE FUNCTIONS ===================
function add_show_remove_visit_listener(){
    $('#delete_visit_patients_btn').click(function(){
        rm_click_listener('.patient-visit');
        show_remove_cancel_btn();
    });
}

function show_remove_cancel_btn(){
    $('#delete_btn_group').show();
    
    $('#delete_visit_patients_btn').click(function(){
        alert(1);
    });
    $('#cancel_delete_visit_patients_btn').click(function(){
        alert(1);
    });
}
//=================== END REMOVE FUNCTIONS ===================
