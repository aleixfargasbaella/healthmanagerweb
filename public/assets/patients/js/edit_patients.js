$(document).ready(function () {
    $("#form_edit_patient").submit(function(e){
        e.preventDefault();
        // var formSerialize = $(this).serialize();
        var formData = new FormData();
        $('input, select, textarea').each(function(){
            var attr = $(this).attr('name');
            var val = $(this).val();
            var input = $("#photo");
            if (typeof attr !== typeof undefined && attr !== false) {
                if (attr == 'photo'){
                    if (file) {
                        formData.append(attr, file);
                    }
                } else {
                    if(val != null && val != ""){
                        formData.append(attr, val);
                    }
                }
            }
        })

        // Display the key/value pairs
        // for (var pair of formData.entries()) {
        //     console.log(pair[0]+ ', ' + pair[1]);
        // }

        $.ajax({
           url: $(this).attr('url'),
           type: 'POST',
           data:  formData,
           dataType: 'json',
           mimeType:"multipart/form-data",
           contentType: false,
           cache: false,
           processData:false,
           success: function(response){
                if(response.status == 'success'){
                    window.location.href = response.action;
                } else {
                    swal(
                        Translator.trans('title_error'), //'Error!',
                        response.action,
                        'error'
                    );
                }
            },
            error: function (xhr, status, errorThrown) {
                swal(
                    Translator.trans('title_error'), //'Error!',
                    errorThrown,
                    'error'
                );
            }
        });
    });
});
