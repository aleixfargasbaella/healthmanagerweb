$(document).ready(function () {
    $('.table-row > :not(.bs-checkbox input)').on('mousedown', function(e) {
        var url = $(this).parent('tr').attr('url');
        open_link(e, url);
    });
});
