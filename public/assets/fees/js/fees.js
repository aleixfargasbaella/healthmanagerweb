var fees_selected = [];

$(document).ready(function () {
    add_show_fee_listener();
    show_fees_checkbox();
});

//=================== LIST FUNCTIONS ===================
function add_show_fee_listener(){
    $('.fee > :not(.bs-checkbox)').click(function(){
        var url = $(this).parent('tr').attr('url');
        window.location.href = url;
    });
}
//=================== END LIST FUNCTIONS ===================

//=================== DELETE FUNCTIONS ===================
function show_fees_checkbox(){
    $('#delete_fees').click(function(){
        $('.check_fee').removeClass('hidden');
        rm_click_listener('.fee > :not(.bs-checkbox input)');
        add_checkbox_listener();
        add_all_checkbox_listener();
        add_removeFee_btn_listener();
        add_cancel_removeFee_btn_listener();
    });
}

function checkbox_click(event, checkbox_object){
    if(!$(event.target).closest('input[type="checkbox"]').length > 0){
        //If not checkbox click, change it's value first
        checkbox_object.prop('checked', !$(checkbox_object).prop('checked'));
    }
    return $(checkbox_object).prop('checked');
}

function check_uncheck_fee(fee_row_object, check){
    var fee_id = $(fee_row_object).attr('data-index');
    var checkbox = $(fee_row_object).children('.bs-checkbox').children('input[type=checkbox]');
    if(check === true){
//        alert('selecting ' + fee_id);
        $(fee_row_object).addClass('selected');
        fees_selected.push(fee_id);
    } else{
//        alert('unselecting ' + fee_id);
        $(fee_row_object).removeClass('selected');
        fees_selected = array_pop(fees_selected, fee_id);
    }

//    console.log(fees_selected);
    checkbox.prop('checked', check);
}
function check_uncheck_all_fees(all_checkbox_status){
    $('#check_all_fees input[name=btSelectAll]').prop('checked', all_checkbox_status);
    $('#fees-table .fee').each(function(){
        check_uncheck_fee(this, all_checkbox_status);
    });
}

function add_checkbox_listener(){
    $('#fees-table .fee').click(function(e){
        var status = checkbox_click(e, $(this).children('.bs-checkbox').children('input[type=checkbox]'));
        check_uncheck_fee(this, status);
    })
}

function add_all_checkbox_listener(){
    $('#check_all_fees').click(function(e){
//        alert('SelectAll clicked');
        var all_checkbox_status = checkbox_click(e, $(this).children('input[name=btSelectAll]'));
        check_uncheck_all_fees(all_checkbox_status);
    })
}

function add_removeFee_btn_listener(){
    $('#delete_fees_btn').click(function(e){
        e.preventDefault();

        if(fees_selected.length > 0){
            swal({
                title: Translator.trans('title_sure'), //'Are you sure?',
                text: Translator.trans('text_noRevert'), //"You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d9534f',
//                cancelButtonColor: '#d33',
                confirmButtonText: Translator.trans('button_delete'), //'Delete'
            }).then(function() {
                $.post('/settings/fees/remove', {"fees_array":fees_selected}, function(response){
                    if(response.status == 'success'){
                        swal(
                            Translator.trans('title_deleted'), //'Deleted!',
                            Translator.trans('text_fee_deleted'), //'The fees has been deleted.',
                            'success'
                        );
                        setTimeout(function() {
                            check_uncheck_all_fees(false);
                            window.location.href = response.action;
                        }, 1000);
                    } else {
                        swal(
                            Translator.trans('title_error'), //'Error!',
                            response.action,
                            'error'
                        );
                    }
                },'JSON');
            })
        } else {
            swal(
                Translator.trans('title_error'), //'Error!',
                Translator.trans('text_error_noFeeselected'), //'First select a fee to delete',
                'error'
            );
        }
    });
}

function add_cancel_removeFee_btn_listener(){
    $('#cancel_delete_fees_btn').click(function(e){
        e.preventDefault();
        rm_click_listener('#fees-table .fee');
        rm_click_listener('#check_all_fees');
        rm_click_listener('#delete_fees_btn');

        check_uncheck_all_fees(false);
        add_show_fee_listener();

        $('.check_fee').addClass('hidden');
    });
}
//=================== END DELETE FUNCTIONS ===================