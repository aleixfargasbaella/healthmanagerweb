$(document).ready(function () {
    current_view = "visits-week-list";
    go_to_next = go_to_week;

    followScroll();
    scrollWithMouse();
    // scrollLeftToToday();
    additional_show_visits_checkbox();
    set_hover_hours_listener();
});

function followScroll(){
    // $('.visits-table-scrollable-horitzontal').on('scroll', function () {
    //     $('.follow-scrollable-horitzontal').scrollLeft($(this).scrollLeft());
    // });
    // $('.follow-scrollable-horitzontal').on('scroll', function () {
    //     $('.visits-table-scrollable-horitzontal').scrollLeft($(this).scrollLeft());
    // });

    $('.visits-table-scrollable-vertical').on('scroll', function () {
        $('.follow-scrollable-vertical').scrollTop($(this).scrollTop());
    });
    $('.follow-scrollable-vertical').on('scroll', function () {
        $('.visits-table-scrollable-vertical').scrollTop($(this).scrollTop());
    });
}

function scrollWithMouse(){
    $('.visits-table-scrollable-vertical').on( 'mousewheel DOMMouseScroll', function (e) {
        var e0 = e.originalEvent;
        var delta = e0.wheelDelta || -e0.detail;

        this.scrollTop += (delta < 0 ? 1 : -1)*30;
        e.preventDefault();
    });
}
//
// function scrollLeftToToday(){
//     var lastElementTop = $('.visit-today').position().left;
//     var scrollAmount = lastElementTop - 200 ;
//
//     $('.visits-table-scrollable-horitzontal').scrollLeft(scrollAmount);
// }

function additional_show_visits_checkbox(){
    $('#delete_visits').click(function(){
        $('.additional-hide-when-removing').addClass('hidden');
        additional_add_cancel_removeVisit_btn_listener();
    });
}

function additional_add_cancel_removeVisit_btn_listener(){
    $('#cancel_delete_visits_btn').click(function(e){
        $('.additional-hide-when-removing').removeClass('hidden');
    });
}

function go_to_week(date){

    if(!$('#visit_toolbar_delete').hasClass('hidden')){
        $('#visit_toolbar_delete').addClass('hidden');
    }
    if(!$('.check_visit').hasClass('hidden')){
        // same as cancel
        rm_click_listener('.visits-table .visit');
        rm_click_listener('#check_all_visits');
        rm_click_listener('#delete_visits_btn');

        check_uncheck_all_visits(false);

        $('.check_visit').addClass('hidden');
        $('.additional-hide-when-removing').removeClass('hidden');
    }

    // $('.visits-month-title').html('');
    $('#visits-week-list').html('');
    $('#loading-gif').removeClass('hidden');
    $.ajax({
        url: '/visits/fetch/week/',
        data: {'new_date': date},
        type: 'POST',
        dataType: 'json',
        beforeSend:function(){

        },
        success: function(response){
            $('#loading-gif').addClass('hidden');
            if(response.status = 'success'){
                if(response.results > 0){
                    $('#visit_toolbar_delete').removeClass('hidden');
                }
                $('#visits-week-list').html(response.action);
                current_day = $('#current_day').val();
                if(date == 'today'){
                    date = current_day;
                }
                add_show_visit_listener();
                rm_click_listener('#go_today')
                next_previous_listeners();
                set_hover_hours_listener();
                followScroll();
                scrollWithMouse();
            } else {
                $('#visits-week-list').html(Translator.trans('error_loading_visits'));
            }
        },
        error: function(){
            $('#loading-gif').addClass('hidden');
            $('#visits-week-list').html(Translator.trans('error_loading_visits'));
            console.log('OUPS!, Something went incredibly wrong changing the visit tables...');
        }
    });
}

function set_hover_hours_listener(){
    var hour;
    var day;
    var hourRow;
    var hourTd;
    var dayDiv;

    $('.table-row').each(function(){
        // $('.visits-table').index(this);
        $(this).mouseover(function(){
            hour = $(this).attr('data-hour');
            day = $(this).attr('data-day');

            hourTr = $(".visit-hour-row[data-hour='" + hour + "']");
            hourTd = $(".visit-hour-row[data-hour='" + hour + "'] > td");
            dayDiv = $(".visits-table-header[data-day='" + day + "']")

            hourTr.addClass('visit-hour-row-hover');
            hourTd.addClass('visit-hour-row-table-text-hover');
            dayDiv.addClass('visit-day-row-hover');
        });

        $(this).mouseout(function(){
            hour = $(this).attr('data-hour');
            day = $(this).attr('data-day');

            hourRow = $(".visit-hour-row[data-hour='" + hour + "']");
            hourTd = $(".visit-hour-row[data-hour='" + hour + "'] > td");
            dayDiv = $(".visits-table-header[data-day='" + day + "']")

            hourRow.removeClass('visit-hour-row-hover');
            hourTd.removeClass('visit-hour-row-table-text-hover');
            dayDiv.removeClass('visit-day-row-hover');
        });
    });
}
