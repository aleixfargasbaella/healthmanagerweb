var patient_name_value = "";
var last_patient_name_value = "";
var patient_phone_value = "";
var last_patient_phone_value = "";
var existing_patient = 0;
var last_existing_patient = 0;

/**
 * Call this method on document ready.
 */
function create_new_patient_click_listener() {
    rm_click_listener('#modal_createNewPatient_div');
    $('#modal_createNewPatient_div').click(function () {
        var checkBox = $("#modal_createNewPatient");
        checkBox.prop("checked", !checkBox.prop("checked"));

        var last_patient_data = changePatientNewToOld(last_patient_name_value, last_patient_phone_value, last_existing_patient);
        last_patient_name_value = last_patient_data.name;
        last_patient_phone_value = last_patient_data.phone;
        last_existing_patient = last_patient_data.id;
    });
}
/**
 * Call this method When going to display the value (in modal on show), if in main page on document ready.
 */
function initPatientInput(isEdit) {
    isEdit = isEdit || false;
    $('#modal_createNewPatient').prop('checked', false);

    $('#modal_patient_name_input').prop('disabled', true);
    $('#modal_patient_name_input').val('');
    $('#modal_patient_phone_input').prop('disabled', true);
    $('#modal_patient_phone_input').val('');
    $('#modal_visit_reason').val('');

    if(!isEdit) {
        $('#modal_patient').prop('disabled', false);
        $('#modal_patient').val(0);
        $('.selectpicker').selectpicker('refresh');
    }

    clearPatientData();
}

function clearPatientData()
{
    patient_name_value = "";
    last_patient_name_value = "";
    patient_phone_value = "";
    last_patient_phone_value = "";
    existing_patient = 0;
    last_existing_patient = 0;
}


function changePatientNewToOld(last_patient_name_value, last_patient_phone_value, last_existing_patient){
    $('#modal_patient').prop('disabled', function(i, v) { return !v; });

    existing_patient = $('#modal_patient').val();
    $('#modal_patient').val(last_existing_patient);
    last_existing_patient = existing_patient;

    $('.selectpicker').selectpicker('refresh');

    $('#modal_patient_name_input').prop('disabled', function(i, v) { return !v; });
    $('#modal_patient_name_input').prop('required', function(i, v) { return !v; });
    $('#modal_patient_phone_input').prop('disabled', function(i, v) { return !v; });
    $('#modal_patient_phone_input').prop('required', function(i, v) { return !v; });

    patient_name_value = $('#modal_patient_name_input').val();
    patient_phone_value = $('#modal_patient_phone_input').val();

    $('#modal_patient_name_input').val(last_patient_name_value);
    $('#modal_patient_phone_input').val(last_patient_phone_value);

    last_patient_name_value = patient_name_value;
    last_patient_phone_value = patient_phone_value;

    return {
        'name': last_patient_name_value,
        'phone': last_patient_phone_value,
        'id': last_existing_patient
    };
}