$(document).ready(function () {
    current_view = "visits-search";
    go_to_next = go_to_search;

    additional_show_visits_checkbox();
});

//=================== LIST FUNCTIONS ===================

function additional_show_visits_checkbox(){
    $('#delete_visits').click(function(){
        $('.additional-hide-when-removing').addClass('hidden');
        additional_add_cancel_removeVisit_btn_listener();
    });
}

function additional_add_cancel_removeVisit_btn_listener(){
    $('#cancel_delete_visits_btn').click(function(e){
        $('.additional-hide-when-removing').removeClass('hidden');
    });
}

function go_to_search(date){
    location.reload();
}
