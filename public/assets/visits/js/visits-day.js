$(document).ready(function () {
    current_view = "visits-list";
    dp = $('#datetimepicker_visits');
    go_to_next = go_to_date;
//    alert(current_day);

    create_datetimepicker_visits();
});

//=================== LIST FUNCTIONS ===================
function create_datetimepicker_visits(){
    var date_to_go;
    var current_date_moment = moment(current_day);

    dp.datetimepicker({
        locale: moment.locale(get_locale()),
        inline: true,
        date: current_date_moment,
        defaultDate: current_date_moment,
        format: 'YYYY-M-D'
    }).on('dp.change', function(event) {
        if(event.oldDate != null){
            if(keydown_press === false){
                date_to_go = event.date.format('YYYY-MM-DD');
                go_to_date(date_to_go);
//            window.location.href = "/visits/list/" + date_to_go;
            }
            else {
                keydown_press = false;
            }
        }
    });
}

function change_datetimepicker_date(date){
    dp.data("DateTimePicker").date(date);
    dp.data("DateTimePicker").viewDate(date);
}

function go_to_date(date){
    check_uncheck_all_visits(false);
    if(!$('#visit_toolbar_delete').hasClass('hidden')){
        $('#visit_toolbar_delete').addClass('hidden');
    }
    $('#list_table_visits').html('');
    $('#loading-gif').removeClass('hidden');
    $.ajax({
        url: '/visits/fetch/visits/',
        data: {'new_date': date},
        type: 'POST',
        dataType: 'json',
        beforeSend:function(){

        },
        success: function(response){
            if(response.status = 'success'){
                if(response.results > 0){
                    $('#visit_toolbar_delete').removeClass('hidden');
                }
                $('#loading-gif').addClass('hidden');
                $('#list_table_visits').html(response.action);
                current_day = $('#current_day').val();
                if(date == 'today'){
                    date = current_day;
                }
                add_show_visit_listener();
                next_previous_button_listener();
                next_previous_keyboard_listeners();
                change_datetimepicker_date(date);
            } else {
                $('#list_table_visits').html(response.error);
            }
        },
        error: function(){
            $('#loading-gif').addClass('hidden');
            $('#list_table_visits').html(Translator.trans('error_loading_visits'));
            console.log('OUPS!, Something went incredibly wrong changing the visit tables...');
        }
    });
}
