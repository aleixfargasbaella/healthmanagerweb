var visits_selected = [];
var current_day = "";
var current_view = "";
var keydown_press = false;
var dp;
var go_to_next;

$(document).ready(function () {
    current_day = $('#current_day').val();

    next_previous_listeners();
    add_show_visit_listener();
    show_visits_checkbox();
});

function next_previous_listeners(){
    next_previous_button_listener();
    next_previous_keyboard_listeners();
}

function next_previous_button_listener(){
    $('#go_previous').click(function(e){
        e.stopPropagation();
        go_previous();
        return false;
    });
    $('#go_today').click(function(e){
        e.stopPropagation();
        go_today();
        return false;
    });
    $('#go_next').click(function(e){
        e.stopPropagation();
        go_next();
        return false;
    });
}

function next_previous_keyboard_listeners(){
    $(document).keydown(function(e) {
        e.stopImmediatePropagation();
        $(this).unbind('keydown');
        keydown_press = true;
        switch(e.which) {
            case 37: // left
                go_previous();
                break;

            case 39: // right
                go_next();
                break;

            default: return;
        }
    });
}

function go_today(){
    go_to_next("today");
}

function go_previous(){
    $('#go_previous').addClass('active');
    go_to_next($('#go_previous').attr('date'));
}

function go_next(){
    $('#go_next').addClass('active');
    go_to_next($('#go_next').attr('date'));
}

function add_show_visit_listener(){
    $('.visit > :not(.bs-checkbox input)').on('mousedown', function(e) {
        var url = $(this).parent('tr').attr('url');
        open_link(e, url+'?fp='+current_view);
    });

    var visitDayText = null;
    var visitDay = null;
    var visitHour = null;

    $('.visit-empty').on('click', function(e) {
        e.preventDefault();
        var dayText = $(this).data('data-daytext');
        var day = $(this).data('data-day');
        var hour = $(this).data('data-hour');

        var visitDayText = $(this).data('daytext');
        visitDay = $(this).data('day');
        var visitHour_string = $(this).data('hour') + ':00';
        var visitHour = $(this).data('hour');

        var visit_dateTime = moment(visitDay);
        visit_dateTime.hour(visitHour);
        var visit_dateTimeString = visit_dateTime.format('YYYY-MM-DD HH:mm:ss');

        $('#modal_add_new_visit').find('.modal-body #modal_visit_dateTime').val(visit_dateTimeString);
        $('#modal_add_new_visit').find('.modal-body #modal_visit_dateTime').attr("value", visit_dateTimeString);
        $('#modal_add_new_visit').find('.modal-body #modal_visit_day').val(visitDayText);
        $('#modal_add_new_visit').find('.modal-body #modal_visit_hour').val(visitHour_string);

        $('#modal_add_new_visit').modal('show');
    });

    create_new_patient_click_listener();

    $('#modal_add_new_visit').on('show.bs.modal', function (event) {
        remove_scroll_to_top();

        var modal = $(this)

        initPatientInput();

        $('#modal_addNewPatientForm').unbind('submit');
        $('#modal_addNewPatientForm').submit(function(e){
            var arrayData = $(this).serializeArray();
            var postData = JSON.stringify(arrayData);
            var formSerialize = $(this).serialize();

            if(arrayData.length > 3 && arrayData[3].name == 'phone'){
                arrayData[3].value.replace(/\s/g,'');
            }

            $.post($(this).attr('url'), formSerialize, function(response){
                if(response.status == 'success'){
                    go_to_next(visitDay);
                    modal.modal('hide')
                } else {
                    swal('Error', response.action, 'error');
                }
            },'JSON').fail(function() {
                swal('Error', 'OUPS!, Something went incredibly wrong adding the new visit...', 'error');
                console.log('OUPS!, Something went incredibly wrong adding the new visit...');
            });

            e.preventDefault();
        });
    })
}

//=================== END LIST FUNCTIONS ===================

//=================== DELETE FUNCTIONS ===================
function show_visits_checkbox(){
    $('#delete_visits').click(function(){
        $('.check_visit').removeClass('hidden');
        rm_click_listener('.visit > :not(.bs-checkbox input)');
        rm_click_listener('.visit-empty');
        add_checkbox_listener();
        add_all_checkbox_listener();
        add_removeVisit_btn_listener();
        add_cancel_removeVisit_btn_listener();
    });
}

function checkbox_click(event, checkbox_object){
    if(!$(event.target).closest('input[type="checkbox"]').length > 0){
        checkbox_object.prop('checked', !$(checkbox_object).prop('checked'));
    }
    return $(checkbox_object).prop('checked');
}

function check_uncheck_visit(visit_row_object, check){
    var visit_id = $(visit_row_object).attr('data-index');
    var checkbox = $(visit_row_object).children('.bs-checkbox').children('input[type=checkbox]');
    if(check === true){
        $(visit_row_object).addClass('selected');
        visits_selected.push(visit_id);
    } else{
        $(visit_row_object).removeClass('selected');
        visits_selected = array_pop(visits_selected, visit_id);
    }

    checkbox.prop('checked', check);
}
function check_uncheck_all_visits(all_checkbox_status){
    $('#check_all_visits input[name=btSelectAll]').prop('checked', all_checkbox_status);
    $('.visits-table .visit').each(function(){
        check_uncheck_visit(this, all_checkbox_status);
    });
}

function add_checkbox_listener(){
    $('.visits-table .visit').click(function(e){
        var status = checkbox_click(e, $(this).children('.bs-checkbox').children('input[type=checkbox]'));
        check_uncheck_visit(this, status);
    })
}

function add_all_checkbox_listener(){
    $('#check_all_visits').click(function(e){
        var all_checkbox_status = checkbox_click(e, $(this).children('input[name=btSelectAll]'));
        check_uncheck_all_visits(all_checkbox_status);
    })
}

function add_removeVisit_btn_listener(){
    $('#delete_visits_btn').click(function(e){
        e.preventDefault();
        if(visits_selected.length > 0){
            swal({
                title: Translator.trans('title_sure'), //'Are you sure?',
                text: Translator.trans('text_noRevert'), //"You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d9534f',
                confirmButtonText: Translator.trans('button_delete'), //'Delete'
            }).then(function() {
                $.post('/visits/remove', {"visits_array":visits_selected, "current_day":current_day}, function(response){
                    if(response.status == 'success'){
                        swal(
                            Translator.trans('title_deleted'), //'Deleted!',
                            Translator.trans('text_visit_deleted'), //'The visits has been deleted.',
                            'success'
                        );
                        setTimeout(function() {
                            check_uncheck_all_visits(false);
                            go_to_next(current_day);
                        }, 1000);
                    } else {
                        swal(
                            Translator.trans('title_error'),
                            response.action,
                            'error'
                        );
                    }
                },'JSON');
            })
        } else {
            swal(
                Translator.trans('title_error'),
                Translator.trans('text_error_noVisitSelected'),
                'error'
            );
        }
    });
}

function add_cancel_removeVisit_btn_listener(){
    $('#cancel_delete_visits_btn').click(function(e){
        e.preventDefault();
        rm_click_listener('.visits-table .visit');
        rm_click_listener('#check_all_visits');
        rm_click_listener('#delete_visits_btn');

        check_uncheck_all_visits(false);
        add_show_visit_listener();

        $('.check_visit').addClass('hidden');
    });
}
//=================== END DELETE FUNCTIONS ===================
